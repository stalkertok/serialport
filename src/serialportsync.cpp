#include "serialportsync.h"
#include <boost/bind.hpp>
#include <QDebug>
#include <memory>


serialPortSync::serialPortSync(unsigned port, int rate, unsigned int character_size, serial_port_base::parity parity, serial_port_base::flow_control flow_control, serial_port_base::stop_bits stop_bits):device("COM"+std::to_string(port)), io(),sp(io),timer(io),rate(rate),character_size(character_size),parity(parity),flow_control(flow_control),stop_bits(stop_bits){

}

bool serialPortSync::open(){
    boost::system::error_code er;
    sp.open(device,er);
    if (er)
        return false;
    sp.set_option(boost::asio::serial_port::baud_rate(rate),er);
    if (er)
        return false;

    sp.set_option(boost::asio::serial_port::character_size(character_size),er);
    if (er)
        return false;
    sp.set_option(boost::asio::serial_port::parity(parity),er);
    if (er)
        return false;
    sp.set_option(boost::asio::serial_port::flow_control(flow_control),er);
    if (er)
        return false;
    sp.set_option(boost::asio::serial_port::stop_bits(stop_bits),er);
    if (er)
        return false;
    return true;
}


bool serialPortSync::isOpen(){
    return sp.is_open();
}

void serialPortSync::close(){
    sp.close();
}

std::string serialPortSync::portName(){
    return device;
}

void serialPortSync::timeout_expired(const boost::system::error_code & error)
{
    if(m_read_result != resultInProgress)
        return;
    if(error != boost::asio::error::operation_aborted)
        m_read_result = resultTimeoutExpired;
}

void serialPortSync::read_completed(const boost::system::error_code& error, const size_t transferred)
{
    if(error)
    {
        if(error != boost::asio::error::operation_aborted)
            m_read_result = resultError;
    }
    else
    {
        if(m_read_result != resultInProgress)
            return;
        m_read_result = resultSuccess;
        this->bytes_transferred = transferred;
    }
}

size_t serialPortSync::read(char *data,size_t minByteTransfered,size_t readBytes, size_t timeWait){

    if (!this->isOpen())
        return 0;

    timer.expires_from_now(boost::posix_time::milliseconds(timeWait));

    timer.async_wait(boost::bind(&serialPortSync::timeout_expired,
                                 this,
                                 boost::asio::placeholders::error
                                 ));

    boost::asio::async_read(sp,
                            boost::asio::buffer(data,readBytes),
                            boost::asio::transfer_at_least(minByteTransfered),
                            boost::bind(&serialPortSync::read_completed,
                                        this,
                                        boost::asio::placeholders::error,
                                        boost::asio::placeholders::bytes_transferred
                                        ));

    m_read_result = resultInProgress;
    bytes_transferred = 0;

    bool exit=false;

    while(!exit)
    {
        io.run_one();
        switch(m_read_result)
        {
        case resultSuccess:
            exit=true;
            break;
        case resultTimeoutExpired:
            sp.cancel();
            exit=true;
            break;
        case resultError:
            sp.cancel();
            timer.cancel();
            exit=true;
            break;
        default:
            break;
        }
    }
   return bytes_transferred;
}

size_t serialPortSync::write(const std::string &data){
    boost::system::error_code er;

    if (!this->isOpen())
        return 0;

    size_t bw=boost::asio::write(sp,boost::asio::buffer(data.c_str(), data.size()),er);

    return er?0:bw;
}
