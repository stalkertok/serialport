#include <QString>
#include <QtTest>
#include <../controlcontroller.h>
#include <QByteArray>

class SerialPortTestTest : public QObject
{
    Q_OBJECT

public:
    SerialPortTestTest();

private Q_SLOTS:
    void testMake_data();
    void testMake();
};

SerialPortTestTest::SerialPortTestTest()
{
}

void SerialPortTestTest::testMake_data()
{
    QTest::addColumn<int>("trk");
    QTest::addColumn<char>("operation");
    QTest::addColumn<int>("price");
    QTest::addColumn<int>("volume");
    QTest::addColumn<QString>("error");
    QTest::addColumn<QString>("state");
    QTest::addColumn<QByteArray>("byteArray");
    QTest::newRow("1") <<1<<'1'<<123456<<123456<<"01"<<"00"<<QByteArray("0130313102313233343536313233343536303130300330");
    QTest::newRow("2") <<2<<'2'<<4569<<8965<<"02"<<"01"<<QByteArray("013032320230303435363930303839363530323031033e");
    QTest::newRow("3") <<3<<'3'<<0<<123456<<"03"<<"02"<<QByteArray("0130333302303030303030313233343536303330320337");
    QTest::newRow("4") <<4<<'4'<<56<<123456<<"04"<<"03"<<QByteArray("0130343402303030303536313233343536303430330332");
    QTest::newRow("5") <<255<<'5'<<77<<123456<<"05"<<"04"<<QByteArray("0146463502303030303737313233343536303530340332");
    QTest::newRow("6") <<159<<'6'<<963<<123456<<"06"<<"05"<<QByteArray("0139463602303030393633313233343536303630350340");
    QTest::newRow("7") <<128<<'7'<<788899<<123456<<"07"<<"06"<<QByteArray("0138303702373838383939313233343536303730360337");
}

void SerialPortTestTest::testMake()
{
    QFETCH(int, trk);
    QFETCH(char, operation);
    QFETCH(int, price);
    QFETCH(int, volume);
    QFETCH(QString, error);
    QFETCH(QString, state);
    QFETCH(QByteArray, byteArray);
    QByteArray actual(ControlController::Make::make(trk,operation,price,volume,error.toStdString().c_str(),state.toStdString().c_str()));
    QCOMPARE(actual.toHex(),byteArray);
}

QTEST_MAIN(SerialPortTestTest)

#include "tst_serialporttesttest.moc"
