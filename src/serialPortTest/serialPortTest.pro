#-------------------------------------------------
#
# Project created by QtCreator 2016-03-17T15:07:42
#
#-------------------------------------------------

QT       += testlib
QT       -= gui
QT       += serialport

TARGET = tst_serialporttesttest
CONFIG   += console
CONFIG   -= app_bundle
CONFIG += c++11

QMAKE_LFLAGS_RELEASE += -static -static-libgcc

TEMPLATE = app

SOURCES += tst_serialporttesttest.cpp \
    ../controlcontroller.cpp
DEFINES += SRCDIR=\\\"$$PWD/\\\"

HEADERS += \
    ../controlcontroller.h
