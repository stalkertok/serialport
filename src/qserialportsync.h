#ifndef SYNCSERIALPORT_H
#define SYNCSERIALPORT_H

#include <QSerialPort>
#include <memory>

const QString COM= "\\\\.\\COM";

class QSerialPortSync:public QSerialPort{

public:
    QSerialPortSync();
    QSerialPortSync(int port, int br=Baud9600);
	QSerialPortSync(const QSerialPortSync&) = delete;
	const QSerialPortSync operator =(const QSerialPortSync&) = delete;
    QByteArray read(qint64 readBytes, int timeWait = 1, int maxTimeWait = 50);
    quint64 write(const char *data, qint64 len, int timeWait = 150);
    ~QSerialPortSync();
private:

};

#endif // SYNCSERIALPORT_H
