#ifndef CONTROLCONTROLLER_H
#define CONTROLCONTROLLER_H

#include <map>
#include "json.hpp"
#include "serialportsync.h"

namespace ControlController {

using json = nlohmann::json;

const unsigned short LENGTH_MESSAGE=23;

const char SOH = 0x01;
const char STX = 0x02;
const char ETX = 0x03;

namespace Command {

const char DOSE           = 0x31;
const char SETUP_TRK_PARAM= 0x33;
const char TEST           = 0x34;
const char RUN            = 0x35;
const char STOP           = 0x36;
const char RESET          = 0x37;
const char FULL           = 0x39;
const char EX_TEST        = 0x54;
const char VERSION        = 0x55;
const char N_DOSE         = 0x56;
const char PARAM_TRK      = 0x38;

static const std::map<char,std::string> commands= {
    {DOSE ,"DOSE\0"},
    {SETUP_TRK_PARAM,"SETUP TRK PARAM\0"},
    {TEST,"TEST\0"},
    {RUN,"RUN\0"},
    {STOP,"STOP\0"},
    {RESET,"RESET\0"},
    {FULL,"FULL\0"},
    {EX_TEST,"EX TEST\0"},
    {VERSION,"VERSION\0"},
    {N_DOSE,"NUMBER DOSE\0"},
    {PARAM_TRK,"PARAM_TRK\0"}
};
}

namespace JSONnames {
namespace Key {
const char OPERATION[]            = {"Operation"};
const char TEXT_ERROR_OPERATION[] = {"textOperationError"};
const char ID[]                   = {"Id"};
const char NUMBER_TRK[]           = {"numberTRK"};
const char CODE_REPLY_ON[]        = {"codeReplyOn"};
const char TEXT_REPLY_ON[]        = {"textReplyOn"};
const char PRICE[]                = {"price"};
const char VOLUME[]               = {"volume"};
const char CODE_ERROR[]           = {"codeError"};
const char CODE_STATE_TRK[]       = {"codeStateTRK"};
const char TEXT_ERROR[]           = {"textError"};
const char TEXT_STATE_TRK[]       = {"textStateTRK"};
const char P1[]                   = {"P1"};
const char P2[]                   = {"P2"};
const char P3_P4[]                = {"P3_P4"};
const char P5_P6[]                = {"P5_P6"};
const char REMOVED[]              = {"removed"};
const char CONNECTION_TRK[]        = {"connectionTRK"};
}

namespace Value {
const char ERROR_OPERATION[]  = {"error"};
const char SUCCES_OPERATION[] = {"succes"};
}
}

namespace Error {

const char NO_ERRORS[]            = {"00"};
const char NO_VALID_TRK[]         = {"01"};
const char NO_VALID_COMMAND_TRK[] = {"02"};
const char NO_VALID_CRC[]         = {"03"};

namespace exErrors {
const char NO_VALID_RETURN[] = {"error, return 0 liters"};
const char NO_VALID_STATUS[] = {"invalid status of the TRK"};
const char NO_WRITE_PORT[]   = {"no write port"};
const char NO_READ_PORT[]    = {"no read port (no connected TRK)"};
const char NO_OPEN_PORT[]    = {"no open port"};
}


static const std::map<std::string,std::string> errors= {
    {NO_ERRORS,"no errors"},
    {NO_VALID_TRK,"invalid number TRK"},
    {NO_VALID_COMMAND_TRK,"invalid command TRK for a given state of TRK"},
    {NO_VALID_CRC,"incorrect checksum"}
};
}

namespace State{

const char NO_STATE[]     = {"00"};
const char READY_RUN[]    = {"01"};
const char DOSE_BALANCE[] = {"02"};
const char RUN[]          = {"03"};
const char STOP[]         = {"04"};
const char RESET[]        = {"05"};
const char FAIL[]         = {"06"};
const char FULL[]         = {"07"};
const char TOTALCOUNTER[] = {"18"};

static const std::map<const std::string,const std::string> states= {
    {NO_STATE,"no state"},
    {READY_RUN,"ready to launch"},
    {DOSE_BALANCE,"dose balance"},
    {RUN,"start"},
    {STOP,"stop"},
    {RESET,"reset (doses not)"},
    {FAIL,"fail"},
    {FULL,"full tank"}
};
}

struct ReturnControllerData{
    ReturnControllerData();
    ~ReturnControllerData();
    short numberTRK;
    char  codeReplyOn;
    std::string textReplyOn;
    unsigned long  price;
    unsigned long  volume;
    char  codeError[3];
    char  codeStateTRK[3];
    std::string textError;
    std::string textStateTRK;
    short P1;
    short P2;
    unsigned int P3_P4;
    unsigned int P5_P6;
    bool removed;
    bool connectionTRK;
};

class Make
{

private:
    static std::string number10ToStr6(unsigned long number);
    static std::string number10ToStr2(short number);

public:
    static char makeCRC(std::string bytearray, unsigned int begin_bit=1, unsigned int end_bit=21);
    static std::string make(short numberTRK, char replyOn, unsigned long price = 0, unsigned long volume = 0, const char *error = Error::NO_ERRORS, const char *state = State::NO_STATE);
    static ReturnControllerData unMake(std::string bytearray);
    static ReturnControllerData unMakeEx(std::string bytearray);
    static ReturnControllerData unMakeForCounter(std::string bytearray);
};

class Work
{
public:
    Work(unsigned port, int rate = 9600);

    enum TypeReturnParam{
        NORMAL       =0,
        EXTENDED     =1,
        TOTALCOUNTER =2,
    };

    bool open();
    bool isOpen();
    void close();

    json inToJson();
    json inToJsonEx();

    ~Work();

    bool test(short numberTRK);
    bool exTest(short numberTRK);
    bool dose(short numberTRK, unsigned long volume, unsigned long price=0);
    bool returnDose(short numberTRK);
    bool run(short numberTRK);
    bool stop(short numberTRK);
    bool reset(short numberTRK);
    bool allStop();
    bool setupState(short numberTRK, const char* state=State::NO_STATE, TypeReturnParam trp=NORMAL);
    bool full(short numberTRK,unsigned long price=0);

    ReturnControllerData data() const;
    void  ClearInData();

    const char *errorChar()const;
    const std::string errorsStdString()const;

private:
    ReturnControllerData inData;
    std::string errors;
    serialPortSync sp;

    void inline errorOpen();
    void inline errorWrite();
    void inline errorRead();
    void inline errorCRC();
    void inline errorError();
    void inline errorVolume();
    void inline errorState();

    bool writeRead(short numberTRK,char replyOn,unsigned long price=0,unsigned long volume=0, const char *error=Error::NO_ERRORS, const char *codeStateTRK=State::NO_STATE,TypeReturnParam trp=NORMAL);
};

}

#endif // CONTROLCONTROLLER_H
