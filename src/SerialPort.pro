#-------------------------------------------------
#
# Project created by QtCreator 2016-02-20T09:11:29
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = SerialPort
CONFIG   += console
CONFIG   -= app_bundle
CONFIG += c++11

TEMPLATE = app

SOURCES += main.cpp \
    controlcontroller.cpp \
    serialportsync.cpp

HEADERS += \
    controlcontroller.h \
    json.hpp \
    serialportsync.h


unix|win32: LIBS += -LC:/local/boost_1_61_0/lib32-msvc-14.0/ -lboost_system-vc140-mt-1_61

INCLUDEPATH += C:/local/boost_1_61_0/
DEPENDPATH += C:/local/boost_1_61_0/
