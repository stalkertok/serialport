#ifndef SERIALPORTSYNC_H
#define SERIALPORTSYNC_H

#include <boost/asio/serial_port.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/asio.hpp>


using namespace boost::asio;

class serialPortSync
{

public:
    serialPortSync(unsigned port, int rate = 9600,
                   unsigned int character_size=8,
                   boost::asio::serial_port::parity parity=boost::asio::serial_port::parity(boost::asio::serial_port::parity::none),
                   boost::asio::serial_port::flow_control flow_control=boost::asio::serial_port::flow_control(boost::asio::serial_port::flow_control::none),
                   boost::asio::serial_port::stop_bits stop_bits=boost::asio::serial_port::stop_bits(boost::asio::serial_port::stop_bits::one));
    bool open();
    bool isOpen();
    void close();
    std::string portName();
    size_t write(const std::string &data);
    size_t read(char *data,size_t minByteTransfered, size_t readBytes, size_t timeWait = 50);

private:
    io_service io;
    serial_port sp;
    boost::asio::deadline_timer timer;
    std::string device;
    int rate;
    unsigned int character_size;
    boost::asio::serial_port::parity parity;
    boost::asio::serial_port::flow_control flow_control;
    boost::asio::serial_port::stop_bits stop_bits;

    enum ReadResult
    {
        resultInProgress = 0,
        resultSuccess = 1,
        resultError = 2,
        resultTimeoutExpired = 3
    };

    ReadResult m_read_result;
    size_t bytes_transferred;

    void timeout_expired(const boost::system::error_code & error);
    void read_completed(const boost::system::error_code &error, const size_t transferred);
};

#endif // SERIALPORTSYNC_H
