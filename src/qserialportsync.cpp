#include "qserialportsync.h"

QSerialPortSync::QSerialPortSync():QSerialPort(){

}

QSerialPortSync::QSerialPortSync(int port, int br):QSerialPort(COM+(QString::number(port))){
    this->setBaudRate(br);

}

QByteArray QSerialPortSync::read(qint64 readBytes, int timeWait, int maxTimeWait){
    
	if (!this->isOpen()) return 0;

    std::unique_ptr<QByteArray> buff(new QByteArray);

    int totalWait = 0;

    forever{
        buff->append(QSerialPort::read(readBytes));

        if (buff->size() >= readBytes || maxTimeWait <= totalWait)
            break;
        totalWait += timeWait;

        QSerialPort::waitForReadyRead(timeWait);
    }

    return std::move(*buff);
}

quint64 QSerialPortSync::write(const char *data, qint64 len, int timeWait){

    if (!this->isOpen()) return 0;
    quint64 bytes = QSerialPort::write(data, len);

    forever{
        if (!QSerialPort::waitForBytesWritten(timeWait))
            break;
    }

    return bytes;
}

QSerialPortSync::~QSerialPortSync(){
}
