#include "controlcontroller.h"
#include <stdio.h>
#include <string.h>
#include <bitset>

using namespace ControlController;

std::string Make::number10ToStr6(unsigned long number){

    if (number<0) number=0;
    if (number>999999) number=999999;

    char data[7];
    sprintf (data,"%06d", (int)number);
    data[6]=0;
    return data;
}

std::string Make::number10ToStr2(short number){

    if (number<=0) number=0;
    if (number>256) number=256;

    char data[3];
    sprintf (data,"%02X", number);
    data[2]=0;
    return data;
}

char Make::makeCRC(std::string bytearray, unsigned int begin_bit, unsigned int end_bit){
    char CRC=0;
    for (unsigned int i=begin_bit; i<=end_bit; i++) CRC^=bytearray[i];
    return CRC;
}


std::string Make::make(short numberTRK, char replyOn, unsigned long price, unsigned long volume, const char* error, const char* state){

    std::string data;
    data.resize(LENGTH_MESSAGE);

    data[0]=::SOH;

    std::string numberTRKStr=number10ToStr2(numberTRK);

    data[1]=numberTRKStr[0];
    data[2]=numberTRKStr[1];

    data[3]=replyOn;

    data[4]=::STX;

    std::string priceStr=number10ToStr6(price);

    std::string volumeStr=number10ToStr6(volume);


    for (int i=0;i<6;i++){
        data[i+5]=priceStr[i];
        data[i+11]=volumeStr[i];
    }

    data[17]=error[0];
    data[18]=error[1];

    data[19]=state[0];
    data[20]=state[1];

    data[21]=::ETX;

    data[22]=makeCRC(data);

    data[23]=0;

    return data;
}

ReturnControllerData Make::unMake(std::string bytearray){

    ReturnControllerData data;

    char numberTRK[3];
    char price[7];
    char volume[7];
    char error[3];
    char state[3];

    numberTRK[0]=bytearray[1];
    numberTRK[1]=bytearray[2];
    numberTRK[2]=0;

    price[0]=bytearray[5];
    price[1]=bytearray[6];
    price[2]=bytearray[7];
    price[3]=bytearray[8];
    price[4]=bytearray[9];
    price[5]=bytearray[10];
    price[6]=0;

    volume[0]=bytearray[11];
    volume[1]=bytearray[12];
    volume[2]=bytearray[13];
    volume[3]=bytearray[14];
    volume[4]=bytearray[15];
    volume[5]=bytearray[16];
    volume[6]=0;

    error[0]=bytearray[17];
    error[1]=bytearray[18];
    error[2]=0;

    state[0]=bytearray[19];
    state[1]=bytearray[20];
    state[2]=0;

    data.numberTRK=(short)strtol(numberTRK,(char**)NULL,16L);

    data.codeReplyOn=bytearray[3];

    data.textReplyOn=Command::commands.at(bytearray[3]);

    data.price=strtol(price,(char**)NULL,10L);

    data.volume=strtol(volume,(char**)NULL,10L);

    strcpy(data.codeError,error);

    strcpy(data.codeStateTRK,state);

    if (Error::errors.count(error))
        data.textError=Error::errors.at(error);

    if (State::states.count(state))
        data.textStateTRK=State::states.at(state);

    return data;
}

ReturnControllerData Make::unMakeEx(std::string bytearray){

    ReturnControllerData data;

    char numberTRK[3];
    char volume[7];
    char error[3];

    numberTRK[0]=bytearray[1];
    numberTRK[1]=bytearray[2];
    numberTRK[2]=0;

    data.P1=(short)strtol(bytearray.substr(5,1).c_str(),(char**)NULL,16L);
    data.P2=(short)strtol(bytearray.substr(6,1).c_str(),(char**)NULL,16L);
    data.P3_P4=(unsigned int)strtol(bytearray.substr(7,2).c_str(),(char**)NULL,16L);
    data.P5_P6=(unsigned int)strtol(bytearray.substr(9,2).c_str(),(char**)NULL,16L);

    volume[0]=bytearray[11];
    volume[1]=bytearray[12];
    volume[2]=bytearray[13];
    volume[3]=bytearray[14];
    volume[4]=bytearray[15];
    volume[5]=bytearray[16];
    volume[6]=0;

    error[0]=bytearray[17];
    error[1]=bytearray[18];
    error[2]=0;


    std::bitset<2> exParam(bytearray[19]);

    data.removed=exParam[0];
    data.connectionTRK=!exParam[1];

    data.numberTRK=(short)strtol(numberTRK,(char**)NULL,16L);

    data.codeReplyOn=bytearray[3];

    data.textReplyOn=Command::commands.at(bytearray[3]);

    data.price=0;

    data.volume=strtol(volume,(char**)NULL,10L);

    strcpy(data.codeError,error);

    data.codeStateTRK[0]=0;
    data.codeStateTRK[1]=0;
    data.codeStateTRK[2]=0;


    if (Error::errors.count(error))
        data.textError=Error::errors.at(error);

    data.textStateTRK=State::NO_STATE;


    return data;

}

ReturnControllerData Make::unMakeForCounter(std::string bytearray){

    ReturnControllerData data;

    char numberTRK[3];
    char volume[9];
    char error[3];
    char state[3];

    numberTRK[0]=bytearray[1];
    numberTRK[1]=bytearray[2];
    numberTRK[2]=0;

    volume[0]=bytearray[9];
    volume[1]=bytearray[10];
    volume[2]=bytearray[11];
    volume[3]=bytearray[12];
    volume[4]=bytearray[13];
    volume[5]=bytearray[14];
    volume[6]=bytearray[15];
    volume[7]=bytearray[16];
    volume[8]=0;

    error[0]=bytearray[17];
    error[1]=bytearray[18];
    error[2]=0;

    state[0]=bytearray[19];
    state[1]=bytearray[20];
    state[2]=0;

    data.numberTRK=(short)strtol(numberTRK,(char**)NULL,16L);

    data.codeReplyOn=bytearray[3];

    data.textReplyOn=Command::commands.at(bytearray[3]);

    data.price=bytearray[5];

    data.volume=strtol(volume,(char**)NULL,16L);

    strcpy(data.codeError,error);

    strcpy(data.codeStateTRK,state);

    if (Error::errors.count(error))
        data.textError=Error::errors.at(error);

    if (State::states.count(state))
        data.textStateTRK=State::states.at(state);

    return data;
}

json Work::inToJson(){

    json jsonObject;

    jsonObject[JSONnames::Key::NUMBER_TRK]= inData.numberTRK;
    jsonObject[JSONnames::Key::CODE_REPLY_ON]= inData.codeReplyOn;
    jsonObject[JSONnames::Key::TEXT_REPLY_ON]=inData.textReplyOn;
    jsonObject[JSONnames::Key::PRICE]= inData.price;
    jsonObject[JSONnames::Key::VOLUME]= inData.volume;
    jsonObject[JSONnames::Key::CODE_ERROR]= inData.codeError;
    jsonObject[JSONnames::Key::CODE_STATE_TRK]= inData.codeStateTRK;
    jsonObject[JSONnames::Key::TEXT_ERROR]= inData.textError;
    jsonObject[JSONnames::Key::TEXT_STATE_TRK]= inData.textStateTRK;

    return jsonObject;
}

json Work::inToJsonEx(){

    json jsonObject=inToJson();
    jsonObject[JSONnames::Key::REMOVED]= inData.removed;
    jsonObject[JSONnames::Key::CONNECTION_TRK]= inData.connectionTRK;

    return jsonObject;

}

Work::~Work(){
}

bool Work::test(short numberTRK){
    ClearInData();
    return writeRead(numberTRK,Command::TEST);
}

bool Work::exTest(short numberTRK){
    ClearInData();
    return writeRead(numberTRK,Command::EX_TEST,0,0,Error::NO_ERRORS,State::NO_STATE,TypeReturnParam::EXTENDED);

}

bool Work::dose(short numberTRK,unsigned long volume,unsigned long price){

    if (!test(numberTRK))
        return false;

    if (strcmp(inData.codeStateTRK,State::RESET)){
        errorState();
        return false;
    }
    ClearInData();

    return writeRead(numberTRK,Command::DOSE,price,volume);
}

bool Work::returnDose(short numberTRK){

    if (!test(numberTRK))
        return false;

    if (inData.volume==0){
        errorVolume();
        return false;
    }

    if (strcmp(inData.codeStateTRK,State::READY_RUN)
            && strcmp(inData.codeStateTRK,State::DOSE_BALANCE)
            && strcmp(inData.codeStateTRK,State::FULL)
            && strcmp(inData.codeStateTRK,State::STOP)){
        errorState();
        return false;
    }
    ClearInData();

    reset(numberTRK);

    return true;
}

bool Work::run(short numberTRK){
    if (!test(numberTRK))
        return false;

    if (strcmp(inData.codeStateTRK,State::READY_RUN) && strcmp(inData.codeStateTRK,State::STOP)){
        errorState();
        return false;
    }
    ClearInData();

    return writeRead(numberTRK,Command::RUN);
}

bool Work::stop(short numberTRK){
    if (!test(numberTRK))
        return false;

    if (strcmp(inData.codeStateTRK,State::RUN)){
        errorState();
        return false;
    }
    ClearInData();

    return writeRead(numberTRK,Command::STOP);
}

bool Work::reset(short numberTRK){
    ClearInData();
    return writeRead(numberTRK,Command::RESET);
}

bool Work::allStop(){
    ClearInData();
    reset(0);
    return true;
}

bool Work::setupState(short numberTRK, const char *state, TypeReturnParam trp){
    ClearInData();

    return writeRead(numberTRK,Command::SETUP_TRK_PARAM,0,0,Error::NO_ERRORS,state,trp);
}

bool Work::full(short numberTRK, unsigned long price){
    if (!test(numberTRK))
        return false;

    if (strcmp(inData.codeStateTRK,State::RESET)){
        errorState();
        return false;
    }

    ClearInData();

    return writeRead(numberTRK,Command::FULL,price,0);
}

ReturnControllerData Work::data() const{
    return inData;
}

void Work::ClearInData(){
    inData.codeError[0]='0';
    inData.codeError[1]='0';
    inData.codeError[2]=0;
    inData.codeReplyOn=0;
    inData.codeStateTRK[0]='0';
    inData.codeStateTRK[1]='0';
    inData.codeStateTRK[0]=0;
    inData.numberTRK=0;
    inData.price=0;
    inData.volume=0;
    inData.P1=0;
    inData.P2=0;
    inData.P3_P4=0;
    inData.P5_P6=0;

    inData.removed=false;
    inData.connectionTRK=false;
    inData.textError.clear();
    inData.textStateTRK.clear();
    inData.textReplyOn.clear();
    this->errors.clear();
}

const char *Work::errorChar() const{
    return errors.c_str();
}

const std::string Work::errorsStdString() const{
    return errors;
}

void Work::errorOpen() {
    errors.append(Error::exErrors::NO_OPEN_PORT);
    errors.append(" ");
    errors.append(sp.portName());
    errors.append("; ");
}

void Work::errorWrite() {
    errors.append(Error::exErrors::NO_WRITE_PORT);
    errors.append(" ");
    errors.append(sp.portName());
    errors.append("; ");
}

void Work::errorRead(){
    errors.append(Error::exErrors::NO_READ_PORT);
    errors.append(" ");
    errors.append(sp.portName());
    errors.append("; ");
}

void Work::errorCRC(){
    errors.append(Error::errors.at(Error::NO_VALID_CRC));
    errors.append("; ");
}

void Work::errorError(){
    errors.append(inData.textError);
    errors.append(" ");
    errors.append("; ");
}

void Work::errorVolume(){
    errors.append(Error::exErrors::NO_VALID_RETURN);
    errors.append(" ");
    errors.append("; ");
}

void Work::errorState(){
    errors.append(Error::exErrors::NO_VALID_STATUS);
    errors.append(" ");
    errors.append("; ");
}

bool Work::writeRead(short numberTRK, char replyOn, unsigned long price, unsigned long volume, const char *error, const char *state, TypeReturnParam trp){

    std::string bytearray = ControlController::Make::make(numberTRK, replyOn, price, volume, error, state);

    if (sp.write(bytearray)<LENGTH_MESSAGE){
        errorWrite();
        return false;
    }

    char buf[LENGTH_MESSAGE];

    auto countBytes=sp.read(buf,LENGTH_MESSAGE,LENGTH_MESSAGE);

    if (countBytes<LENGTH_MESSAGE){
        errorWrite();
        return false;
    }

    std::string answerData(buf,countBytes);

    if (ControlController::Make::makeCRC(answerData)!=answerData[LENGTH_MESSAGE-1]){
        errorCRC();
        return false;
    }

    if (strcmp(inData.codeError,Error::NO_ERRORS)!=0){
        errorError();
        return false;
    }

    switch (trp) {
    case EXTENDED:
        inData=Make::unMakeEx(answerData);
        break;
    case TOTALCOUNTER:
        inData=Make::unMakeForCounter(answerData);
        break;
    case NORMAL:
    default:
        inData=Make::unMake(answerData);
        break;
    }

    return true;
}


Work::Work(unsigned port, int rate):inData(),sp(port,rate){
}

bool Work::open(){
    return sp.open();
}

bool Work::isOpen(){
    return sp.isOpen();
}

void Work::close(){
    sp.close();
}

ReturnControllerData::ReturnControllerData(){
    this->codeError[0]='0';
    this->codeError[1]='0';
    this->codeError[2]=0;
    this->codeReplyOn=0;
    this->codeStateTRK[0]='0';
    this->codeStateTRK[1]='0';
    this->codeStateTRK[0]=0;
    this->numberTRK=0;
    this->price=0;
    this->volume=0;
    this->textError="";
    this->textStateTRK="";
    this->textReplyOn="";
    this->P1=0;
    this->P2=0;
    this->P3_P4=0;
    this->P5_P6=0;
    this->removed=false;
    this->connectionTRK=false;
}

ReturnControllerData::~ReturnControllerData(){

}
